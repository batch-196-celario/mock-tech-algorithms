let collection = [];
// Write the queue functions below.

function enqueue(element) {
    collection[collection.push()] = element
    return collection
}

function print() {
    return collection
}


function dequeue() {
    return collection.shift()
}

function front() {
    return collection[0]
}

function size() {
 return collection.length
}

function isEmpty() {
 return collection.includes()
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
